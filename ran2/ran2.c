#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define IM1 2147483563
#define IM2 2147483399
#define AM (1.0/IM1)
#define IMM1 (IM1-1)
#define IA1 40014
#define IA2 40692
#define IQ1 53668
#define IQ2 52774
#define IR1 12211
#define IR2 3791
#define NTAB 32
#define NDIV (1+IMM1/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

float ran2(long idum)
{
   int j;
   long k;
   static long idum2=123456789;
   static long iy=0;
   static long iv[NTAB];
   float temp;

   if(idum <= 0)
   {
      if(-(idum) < 1) idum=1;
      else idum = -(idum);
      idum2=(idum);

      for(j=NTAB+7;j>=0;j--){
         k=(idum)/IQ1;
         idum=IA1*(idum-k*IQ1)-k*IR1;
         if(idum < 0) idum += IM1;
         if(j < NTAB) iv[j] = idum;
      }
      iy = iv[0];
   }

   k=(idum)/IQ1;
   idum=IA1*(idum-k*IQ1)-k*IR1;

   if(idum < 0) idum += IM1;
   k=idum2/IQ2;
   idum2=IA2*(idum2-k*IQ2)-k*IR2;

   if(idum2 < 0) idum2 += IM2;
   j=iy/NDIV;
      iy=iv[j]-idum2;
      iv[j] = idum;
      if(iy < 1) iy += IMM1;
      if((temp=AM*iy) > RNMX) return RNMX;
      else return temp;
}

#define NBCOLUM 2
#define NBLINE 33554432

int main(int argc, char **argv)
{
fflush(stdout);
printf("BONJOUR");
fflush(stdout);
//  double mat[NBLINE][NBCOLUM]={0};
  char nameFile[100];
  unsigned long long i,j;

  /*for(i = 0; i < NBLINE; i++)
     for(j = 0; j < NBCOLUM; j++)
        mat[i][j] = ran2(1);*/

  sprintf(nameFile,"lcgran2-%d.txt",NBCOLUM);
  FILE *file = fopen(nameFile,"w");

  if(file == NULL)
  {
    printf("Error opening file! \n");
    exit(-1);
  }

  /*for(j = 0; j < NBCOLUM; j++)
     for(i = 0; i < NBLINE; i++)
        fprintf(file, "%10.15f\n", mat[i][j]);*/
  unsigned long long nb_occ = pow(2,18);
	for(i = 0; i < nb_occ; i++){
  		fprintf(file, "status n°%llu: %10.15f\n", i*nb_occ, ran2(1));
		for(j= 1; j<nb_occ; j++){
			ran2(1);
	   	}
	}
  unsigned long long dernier = pow(262144, 2);
  fprintf(file, "status n°%llu: %10.15f\n", dernier, ran2(1));
  fclose(file);

  return 0;
}
