#include <math.h>
#include <iostream>

using namespace std;
typedef unsigned long long int64;

int64 mult(int64, int64);
void reccurence (int64[], int);

int main() {
	/*cout << mult(123456789012345678,987654321098765432) << endl << endl;
	int64 tab[18] = {1,12,42,54,78,13,23,24,10,1332,10,23,125,179,84,354,250,0};
	reccurence (tab, 10);*/
	int64 a = 0, b = 1;
	a[1] = b[1] << 20;
	cout << a << " et " << b << endl;
	return 0;
}

int64 mult (int64 a, int64 b) {
	return (a*b)%((int64)pow(2, 64));
}

void reccurence (int64 tab[], int nbOccurences) {
	int n1 = 12, n2 = 0, n3 = 17;
	for(int i = 0; i < nbOccurences; i++){
		tab[n3] = mult(tab[n1], tab[n2]);
		//cout << tab[n3] << endl;
		n3 = (n3+1) % 17;
		n2 = (n2+1) % 17;
		n1 = (n1+1) % 17;
	}
}
