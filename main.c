#include "mlfg.h"
#include <math.h>
#include <stdio.h>

struct rngen
{
  int init_seed;
  int parameter;
  int narrays;
  int *array_sizes;
  int **arrays;

  unsigned long long *lags;
  unsigned long long *si;
  int hptr;          /* integer pointer into fill */
  int lval, kval, seed;
};

int main() {
	int * x = init_rng(0, 1);
	struct rngen * p = (struct rngen *) x;
	
	int i, j;
	for (i=0; i<60; i++)
	{
		printf("%llu\n", p->lags[i]);
	}
	
	return 0;
}
